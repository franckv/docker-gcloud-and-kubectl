# Docker GCloud And KubeCtl


Publish a new version to Docker Hub
- check versions locally manually
- build locally the image with the right version names inside the image name
- push the created image to Docker Hub


## Commands summary

```
cd ~/Documents/MyFolder/docker-gcloud-and-kubectl

# check latest version locally:
# gcloud components update
# then you will see e.g.:
#     
#     Your current Google Cloud CLI version is: 394.0.0
#     You will be upgraded to version: 407.0.0
#     
#     ┌─────────────────────────────────────────────────────────────────────────────┐
#     │                      These components will be updated.                      │
#     ├─────────────────────────────────────────────────────┬────────────┬──────────┤
#     │                         Name                        │  Version   │   Size   │
#     ├─────────────────────────────────────────────────────┼────────────┼──────────┤
#     │ BigQuery Command Line Tool                          │     2.0.79 │  1.6 MiB │
#     │ BigQuery Command Line Tool (Platform Specific)      │     2.0.77 │  < 1 MiB │
#     │ Cloud Storage Command Line Tool                     │       5.15 │ 15.5 MiB │
#     │ Cloud Storage Command Line Tool (Platform Specific) │       5.13 │  < 1 MiB │
#     │ Google Cloud CLI Core Libraries                     │ 2022.10.21 │ 25.1 MiB │
#     │ Google Cloud CLI Core Libraries (Platform Specific) │ 2022.09.20 │  < 1 MiB │
#     │ anthoscli                                           │     0.2.30 │ 37.9 MiB │
#     │ gcloud Beta Commands                                │ 2022.10.21 │  < 1 MiB │
#     │ gcloud cli dependencies                             │ 2022.10.21 │ 11.3 MiB │
#     │ gke-gcloud-auth-plugin                              │      0.4.0 │  7.1 MiB │
#     │ kubectl                                             │    1.22.14 │ 78.5 MiB │
#     │ kubectl                                             │    1.22.14 │  < 1 MiB │
#     └─────────────────────────────────────────────────────┴────────────┴──────────┘
#     
# see also version by:
#
# gcloud --version
#
#      Google Cloud SDK 407.0.0
#      beta 2022.10.21
#      bq 2.0.79
#      core 2022.10.21
#      gsutil 5.15
#      kubectl 1.22.14
#
#
# if e.g. Google Cloud SDK 407.0.0
# and kubectl 1.22.14
# then:
#
# 1- make sure you download the latest
docker rmi google/cloud-sdk:alpine
#
# 2- build the image locally
docker build --no-cache -t newstartup/docker-gcloud-and-kubectl:gc407.0.0.kctl.1.22.14 .
#
# 3- push to Docker Hub
docker push newstartup/docker-gcloud-and-kubectl:gc407.0.0.kctl.1.22.14

```



## In case you want to locally delete an image you created by error locally :

```
docker rmi newstartup/docker-gcloud-and-kubectl:gc406.0.0.kctl.x.x.x
```



## How to build the image locally ?

```
git clone https://gitlab.com/franckv/docker-gcloud-and-kubectl
cd docker-gcloud-and-kubectl/
docker build -t newstartup/docker-gcloud-and-kubectl:gc294.0.0.kctl.1.15.11 .
```

## Login to Docker Hub account

```
docker login
```

...enter login and password in your local Terminal


## Push locally built image to remote Docker Hub

```
docker push newstartup/docker-gcloud-and-kubectl:gc294.0.0.kctl.1.15.11
```

## Resulting image available from Docker Hub :

```
https://hub.docker.com/repository/docker/newstartup/docker-gcloud-and-kubectl
```

## Using the Docker image in a Dockerfile

The Dockerfile `FROM` is :

```
FROM newstartup/docker-gcloud-and-kubectl:gc294.0.0.kctl.1.15.11
```